﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Extensions;


public class AuthSetup : MonoBehaviour
{
    Firebase.DependencyStatus dependencyStatus;
    public AuthManager authManager;

    private void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                authManager.InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not solve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }
}
