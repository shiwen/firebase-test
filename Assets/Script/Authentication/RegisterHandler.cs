﻿using Firebase.Auth;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Threading.Tasks;
using System;

public class RegisterHandler : MonoBehaviour
{
    public Canvas registerCanvas;
    public Canvas loginCanvas;
    public TMP_InputField emailText;
    public TMP_Text emailErrorText;
    public TMP_InputField passwordText;
    public TMP_InputField confirmPasswordText;
    public TMP_Text passwordErrorText;
    public Button submitButton;
    public Button backBtn;


    private FirebaseAuth auth;
    private string displayName = "";

    void Start()
    {
        auth = FirebaseAuth.DefaultInstance;

        submitButton.onClick.AddListener(canSubmit);

        backBtn.onClick.AddListener(() =>
        {
            //Go back to sign in page
            //SceneManager.LoadScene("SignInScene");
            registerCanvas.enabled = false;
            loginCanvas.enabled = true;
        });
    }

    void canSubmit()
    {
        passwordErrorText.enabled = false;
        if (passwordText.text != confirmPasswordText.text)
        {
            passwordErrorText.text = "Password do not match";
            passwordErrorText.enabled = true;
        }
        else
        {

        }
    }

    // Create a user with the email and password.
    public Task CreateUserWithEmailAsync()
    {
        string email = emailText.text;
        string password = passwordText.text;

        Debug.Log(String.Format("Attempting to create user {0}...", email));
        DisableUI();

        // This passes the current displayName through to HandleCreateUserAsync
        // so that it can be passed to UpdateUserProfile().  displayName will be
        // reset by AuthStateChanged() when the new user is created and signed in.
        return auth.CreateUserWithEmailAndPasswordAsync(email, password)
          .ContinueWithOnMainThread((task) =>
          {
              EnableUI();
              LogTaskCompletion(task, "User Creation");
              return task;
          }).Unwrap();
    }

    public Task UpdateUserProfileAsync(string newDisplayName = null)
    {
        if (auth.CurrentUser == null)
        {
            Debug.Log("Not signed in, unable to update user profile");
            return Task.FromResult(0);
        }

        displayName = newDisplayName ?? displayName;
        Debug.Log("Updating user profile " + displayName);
        return auth.CurrentUser.UpdateUserProfileAsync(new UserProfile
        {
            DisplayName = displayName,
            PhotoUrl = auth.CurrentUser.PhotoUrl,
        });
    }

    protected bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;

        //if task is canceled
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        //if some error occurred, show the error type
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string authErrorCode = "";
                Firebase.FirebaseException firebaseException = exception as Firebase.FirebaseException;
                if (firebaseException != null)
                {
                    authErrorCode = String.Format("AuthError.{0}: ", (AuthError)firebaseException.ErrorCode).ToString();
                    GetErrorMessage((AuthError)firebaseException.ErrorCode);
                }
                Debug.Log(authErrorCode + exception.ToString());
            }
        }
        //if task completed
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed.");
            complete = true;
        }

        return complete;
    }

    void DisableUI()
    {
        emailText.DeactivateInputField();
        passwordText.DeactivateInputField();
        confirmPasswordText.DeactivateInputField();
        backBtn.interactable = false;
        submitButton.interactable = false;
        emailErrorText.enabled = false;
        passwordErrorText.enabled = false;
    }


    void EnableUI()
    {
        emailText.ActivateInputField();
        passwordText.ActivateInputField();
        confirmPasswordText.ActivateInputField();
        backBtn.interactable = true;
        submitButton.interactable = true;
    }

    private void GetErrorMessage(AuthError errorCode)
    {
        switch (errorCode)
        {
            case AuthError.MissingPassword:
                passwordErrorText.text = "Missing password.";
                passwordErrorText.enabled = true;
                break;
            case AuthError.WeakPassword:
                passwordErrorText.text = "Too weak of a password.";
                passwordErrorText.enabled = true;
                break;
            case AuthError.InvalidEmail:
                emailErrorText.text = "Invalid email.";
                emailErrorText.enabled = true;
                break;
            case AuthError.MissingEmail:
                emailErrorText.text = "Missing email.";
                emailErrorText.enabled = true;
                break;
            case AuthError.UserNotFound:
                emailErrorText.text = "Account not found.";
                emailErrorText.enabled = true;
                break;
            case AuthError.EmailAlreadyInUse:
                emailErrorText.text = "Email already in use.";
                emailErrorText.enabled = true;
                break;
            default:
                emailErrorText.text = "Unknown error occurred.";
                emailErrorText.enabled = true;
                break;
        }
    }
}
