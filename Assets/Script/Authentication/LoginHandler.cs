﻿using Firebase.Auth;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Threading.Tasks;
using System;


public class LoginHandler : MonoBehaviour
{
    public Canvas registerCanvas;
    public Canvas loginCanvas;

    public Button loginBtn;
    public Button registerBtn;

    public TMP_InputField emailText;
    public TMP_Text emailErrorText;
    public TMP_InputField passwordText;
    public TMP_Text passwordErrorText;

    public Button backBtn;

    private FirebaseAuth auth;
    // Whether to sign in / link or reauthentication *and* fetch user profile data.
    protected bool signInAndFetchProfile = true;


    void Start()
    {
        auth = FirebaseAuth.DefaultInstance;

        loginBtn.onClick.AddListener(() => { LoginWithEmailAsync(); });

        registerBtn.onClick.AddListener(() =>
        {
            loginCanvas.enabled = false;
            registerCanvas.enabled = true;
        });
    }

    public Task LoginWithEmailAsync()
    {
        var email = emailText.text;
        var password = passwordText.text;

        Debug.Log(String.Format("Attempting to sign in as {0}...", email));


        if (signInAndFetchProfile)
        {
            return auth.SignInAndRetrieveDataWithCredentialAsync(EmailAuthProvider.GetCredential(email, password)).ContinueWithOnMainThread(HandleSignInWithSignInResult);
        }
        else
        {
            return auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(HandleSignInWithSignInWithUser);
        }
    }

    void HandleSignInWithSignInResult(Task<SignInResult> task)
    {
        EnableUI();
        if (LogTaskCompletion(task, "Sign-In"))
        {
            DisplaySignInResult(task.Result, 1);
        }
    }

    void HandleSignInWithSignInWithUser(Task<FirebaseUser> task)
    {
        EnableUI();
        if (LogTaskCompletion(task, "Sign-In"))
        {
            Debug.Log(String.Format("{0} signed in", task.Result.DisplayName));
        }
    }

    // Display user information reported
    protected void DisplaySignInResult(SignInResult result, int indentLevel)
    {
        string indent = new String(' ', indentLevel * 2);
        var metadata = result.Meta;
        if (metadata != null)
        {
            Debug.Log(String.Format("{0}Created: {1}", indent, metadata.CreationTimestamp));
            Debug.Log(String.Format("{0}Last Sign-in: {1}", indent, metadata.LastSignInTimestamp));
        }
        var info = result.Info;
        if (info != null)
        {
            Debug.Log(String.Format(" {0}Additional User Info:", indent));
            Debug.Log(String.Format("{0}  User Name: {1}", indent, info.UserName));
            Debug.Log(String.Format("{0}  Provider ID: {1}", indent, info.ProviderId));
            DisplayProfile<string>(info.Profile, indentLevel + 1);
        }
    }

    protected void DisplayProfile<T>(IDictionary<T, object> profile, int indentLevel)
    {
        string indent = new String(' ', indentLevel * 2);
        foreach (var kv in profile)
        {
            var valueDictionary = kv.Value as IDictionary<object, object>;
            if (valueDictionary != null)
            {
                Debug.Log(String.Format("{0}{1}:", indent, kv.Key));
                DisplayProfile<object>(valueDictionary, indentLevel + 1);
            }
            else
            {
                Debug.Log(String.Format("{0}{1}: {2}", indent, kv.Key, kv.Value));
            }
        }
    }

    void DisableUI()
    {
        emailText.DeactivateInputField();
        passwordText.DeactivateInputField();
        loginBtn.interactable = false;
        registerBtn.interactable = false;
        emailErrorText.enabled = false;
        passwordErrorText.enabled = false;
    }

    void EnableUI()
    {
        emailText.ActivateInputField();
        passwordText.ActivateInputField();
        loginBtn.interactable = true;
        registerBtn.interactable = true;
    }

    // Log the result of the specified task, returning true if the task
    // completed successfully, false otherwise.
    protected bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string authErrorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    authErrorCode = String.Format("AuthError.{0}: ",
                      ((Firebase.Auth.AuthError)firebaseEx.ErrorCode).ToString());
                    GetErrorMessage((Firebase.Auth.AuthError)firebaseEx.ErrorCode);
                }
                Debug.Log(authErrorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    private void GetErrorMessage(AuthError errorCode)
    {
        switch (errorCode)
        {
            case AuthError.MissingPassword:
                passwordErrorText.text = "Missing password.";
                passwordErrorText.enabled = true;
                break;
            case AuthError.WrongPassword:
                passwordErrorText.text = "Incorrect password.";
                passwordErrorText.enabled = true;
                break;
            case AuthError.InvalidEmail:
                emailErrorText.text = "Invalid email.";
                emailErrorText.enabled = true;
                break;
            case AuthError.MissingEmail:
                emailErrorText.text = "Missing email.";
                emailErrorText.enabled = true;
                break;
            case AuthError.UserNotFound:
                emailErrorText.text = "Account not found.";
                emailErrorText.enabled = true;
                break;
            default:
                emailErrorText.text = "Unknown error occurred.";
                emailErrorText.enabled = true;
                break;
        }
    }
}
